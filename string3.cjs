function findMonth(format) {
  const monthSplit = format.split("/");
  //splitting and storing the values in an array

  const target = Number(monthSplit[1]) - 1;
  /*
  since the month will be stored the 1st index of the array
  we convert that to integer and substract 1 from it since 
  the arrays will be zero indexed in the months array.
  */

  const months = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "Augest",
    "September",
    "October",
    "November",
    "December",
  ];

  if (Number(monthSplit[1]) > 12) {
    return "Invalid month. Month should be between 1-12 in the format";
  } else {
    return months[target];
  }
}

module.exports = findMonth;
