function titleCase(person) {
  const firstName = (person.first_name || "").toLowerCase();
  const middleName = (person.middle_name || "").toLowerCase();
  const lastName = (person.last_name || "").toLowerCase();
  //converting the names to lowercase.

  const newFirstName = firstName.charAt(0).toUpperCase() + firstName.slice(1);
  /*
  converting the first character to uppercase and concat with the rest of the
  remaining name.
  */
  const newMiddleName =
    middleName.charAt(0).toUpperCase() + middleName.slice(1);

  const newLastName = lastName.charAt(0).toUpperCase() + lastName.slice(1);

  if (middleName === "") {
    return newFirstName + " " + newLastName;
  } else {
    return newFirstName + " " + newMiddleName + " " + newLastName;
  }
  //if the middlename is not present than we doesnot return middleName to avoid extraSpace.
}

module.exports = titleCase;
