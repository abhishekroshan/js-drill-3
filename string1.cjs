function convertToAbsolute(string) {
  const str = string.split("").filter((word) => {
    return word !== "$";
  });
  //seperating the characters of the string
  //using filter to remove the '&' from the array.

  const newStr = str.reduce((accumulator, currentValue) => {
    return accumulator + currentValue;
  });
  //using reduce to concat the values of the array

  const mainValue = parseFloat(newStr);
  //converting the string to a integer/float.

  return mainValue;
}

module.exports = convertToAbsolute;
