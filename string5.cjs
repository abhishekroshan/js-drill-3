function convertToString(array) {
  if (array.length === 0) {
    return "";
  }
  //returning empty string if the array is empty

  const newString = array.reduce((accumulator, currentValue) => {
    return accumulator + " " + currentValue;
  });
  //using reduce to concat the elements of the array.

  return newString;
}

module.exports = convertToString;
