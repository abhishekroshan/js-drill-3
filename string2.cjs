function convertionOfIp(ipAddress) {
  const newArrayOfIp = ipAddress.split(".");
  //splitting the string in array

  const numericForm = newArrayOfIp.map((nums) => {
    return Number(nums);
  });
  //converting the element of array to numbers.

  const errorCheck = numericForm.filter((nums) => {
    return Number.isFinite(nums);
  });
  //creating an array of numbers so that words and alphanumeric values can be excluded.

  if (numericForm.length === errorCheck.length) {
    return numericForm;
  } else {
    return [];
  }
  /* 
  if the length of numericForm array and errorCheck array is not
  equal then there have been some elements present apart from numbers
  in that case we can return empty array.

  if numericForm Array and errorCheck array are same length 
  then we simply return numericForm array;
  */
}

module.exports = convertionOfIp;
