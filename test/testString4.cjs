const titleCase = require("../string4.cjs");

const person1 = { first_name: "JoHN", last_name: "SMith" };
const person2 = { first_name: "JoHN", middle_name: "doe", last_name: "SMith" };

console.log(titleCase(person1));
console.log(titleCase(person2));
